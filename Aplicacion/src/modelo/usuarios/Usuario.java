package modelo.usuarios;

public class Usuario {
	
	protected Long idUsuario;
	protected String usuario;
	protected String password;
	protected String nombre;
	protected Boolean recibirNotificaciones;
	protected Integer edad;
	
	
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getRecibirNotificaciones() {
		return recibirNotificaciones;
	}
	public String getRecibirNotificacionesAsString() {
		return recibirNotificaciones ? "S" : "N";
	}
	public void setRecibirNotificaciones(Boolean recibirNotificaciones) {
		this.recibirNotificaciones = recibirNotificaciones;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", usuario=" + usuario + ", password=" + password + ", nombre="
				+ nombre + ", recibirNotificaciones=" + recibirNotificaciones + ", edad=" + edad + "]";
	}
	
	
	
	
}
