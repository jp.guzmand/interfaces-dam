package services.usuarios.exception;

public class UsuarioNoExisteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -108975316572708400L;

	public UsuarioNoExisteException() {
	}

	public UsuarioNoExisteException(String arg0) {
		super(arg0);
	}

	public UsuarioNoExisteException(Throwable arg0) {
		super(arg0);
	}

	public UsuarioNoExisteException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public UsuarioNoExisteException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
