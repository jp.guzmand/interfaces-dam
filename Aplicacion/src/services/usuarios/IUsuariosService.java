package services.usuarios;

import modelo.usuarios.Usuario;
import services.usuarios.exception.AccesoDenegadoException;
import services.usuarios.exception.UsuarioNoExisteException;
import services.usuarios.exception.UsuariosServiceException;

public interface IUsuariosService {

	
	/** Permite hacer login con el usuario y password indicado.
	 * @param usuario
	 * @param password
	 * @return
	 * @throws AccesoDenegadoException --> Si el usuario no existe o la password es incorrecta
	 * @throws UsuariosServiceException --> Si se produce cualquier error
	 */
	public Long login(String usuario, String password) throws AccesoDenegadoException, UsuariosServiceException;
	
	
	/** Nos permite consultar todos los datos de un usuarios a partir de su ID
	 * @param idUsuario
	 * @return
	 * @throws UsuarioNoExisteException --> Si el usuario indicado no est� en base de datos
	 * @throws UsuariosServiceException --> Si se produce cualquier error 
	 */
	public Usuario consultarDatosUsuario(Long idUsuario) throws UsuarioNoExisteException, UsuariosServiceException;
	
	
	/** Nos permite actualizar los datos del usuario indicado.
	 * @param usuario
	 * @throws UsuarioNoExisteException --> Si el usuario indicado no existe.
	 * @throws UsuariosServiceException --> Si se produce cualquier otro error
	 */
	public void actualizarUsuario(Usuario usuario) throws UsuarioNoExisteException, UsuariosServiceException;
	
	
}
