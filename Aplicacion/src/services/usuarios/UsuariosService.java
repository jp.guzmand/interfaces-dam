package services.usuarios;

import java.sql.SQLException;

import database.usuarios.UsuariosDatabase;
import modelo.usuarios.Usuario;
import services.usuarios.exception.AccesoDenegadoException;
import services.usuarios.exception.UsuarioNoExisteException;
import services.usuarios.exception.UsuariosServiceException;

public class UsuariosService implements IUsuariosService{

	@Override
	public Long login(String usuario, String password) throws AccesoDenegadoException, UsuariosServiceException {
		
		// 1. Consultar el usuario por campo USUARIO
		try {
			UsuariosDatabase usuariosDB = new UsuariosDatabase();
			Usuario datosUsuarios = usuariosDB.consultarUsuarioPorLogin(usuario);
			
			// 2. Si no existe el usuario --> Lanzar excepci�n AccesoDenegadoException
			if (datosUsuarios == null) {
				throw new AccesoDenegadoException("El usuario " + usuario + " no existe.");
			}
			
			// 3. Comprobar que las password son iguales
			boolean passwordIguales = datosUsuarios.getPassword().equals(password);
			
			// 4. Si no son iguales --> Lanzar excepci�n AccesoDenegadoException
			if (!passwordIguales) {
				throw new AccesoDenegadoException("La password indicada no es correcta ");
			}
			
			// 5. Si son iguales --> return idUsuario
			return datosUsuarios.getIdUsuario();

		}
		catch(SQLException e) {
			throw new UsuariosServiceException("Error al consultar usuario", e);
		}
		
	}

	@Override
	public Usuario consultarDatosUsuario(Long idUsuario) throws UsuarioNoExisteException, UsuariosServiceException {
		try {
			// 1. Consultar el usuario por id
			UsuariosDatabase usuariosDB = new UsuariosDatabase();
			Usuario datosUsuarios = usuariosDB.consultarUsuarioPorId(idUsuario);
			
			// 2. Comprobar si existe 
			if (datosUsuarios == null) {
				// 3. Si no existe --> Excepci�n UsuarioNoExisteException
				throw new UsuarioNoExisteException("El usuario " + idUsuario + " no existe.");
			}
			
			// 4. Si existe, lo devolvemos
			return datosUsuarios;
		}
		catch(SQLException e) {
			throw new UsuariosServiceException("Error al consultar en Base de Datos", e);
		}
	}

	@Override
	public void actualizarUsuario(Usuario usuario) throws UsuarioNoExisteException, UsuariosServiceException {
		try {
			// 1.  Actualizar datos en base datos
			UsuariosDatabase usuariosDB = new UsuariosDatabase();
			Integer numRegistrosActualizados = usuariosDB.actualizarUsuario(usuario);
			
			// 2. Comprobar si no se ha actualizado ninguno. 
			if (numRegistrosActualizados == 0) {
				throw new UsuarioNoExisteException("No se ha actualizado ning�n usuario");
			}
			// 2. Comprobar si se han actualizado m�s de uno
			if (numRegistrosActualizados > 1) {
				throw new UsuariosServiceException("Error al actualizar en Base de Datos. Se han actualizado m�s de un usuario");
			}
		}
		catch(SQLException e) {
			throw new UsuariosServiceException("Error al actualizar en Base de Datos", e);
		}
	}

	
}
