package database.usuarios;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.connection.ProveedorConexiones;
import modelo.usuarios.Usuario;

public class UsuariosDatabase {

	public Usuario consultarUsuarioPorLogin(String usuario) throws SQLException {
		String sql = "SELECT * FROM USUARIO WHERE USUARIO = '" + usuario + "'";
		return consultarUsuario(sql);
	}
	public Usuario consultarUsuarioPorId(Long idUsuario) throws SQLException {
		String sql = "SELECT * FROM USUARIO WHERE ID_USUARIO = " + idUsuario;
		return consultarUsuario(sql);
	}
	
	private Usuario consultarUsuario(String sql) throws SQLException {
		Connection conn = null; 
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				Usuario usuarioResultado = new Usuario();
				usuarioResultado.setIdUsuario(rs.getLong("ID_USUARIO"));
				usuarioResultado.setUsuario(rs.getString("USUARIO"));
				usuarioResultado.setPassword(rs.getString("PASSWORD"));
				usuarioResultado.setNombre(rs.getString("NOMBRE"));
				usuarioResultado.setEdad(rs.getInt("EDAD"));
				String notificaciones = rs.getString("RECIBIR_NOTIFICACIONES");
				usuarioResultado.setRecibirNotificaciones(notificaciones.equals("S"));
				return usuarioResultado;
			}
			// No hay usuario
			return null;
			
		}
		catch(SQLException e) {
			System.err.println("Error al consultar usuario. ");
			System.err.println("Detalle del error:  " + e.getMessage());
			e.printStackTrace();
			throw e;
		}
		finally {
			try {
				rs.close();
			}
			catch(Exception ignore) {}
			try {
				stmt.close();
			}
			catch(Exception ignore) {}
			try {
				conn.close();
			}
			catch(Exception ignore) {}
		}
		
	}
	
	
	public Integer actualizarUsuario(Usuario usuario) throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = new ProveedorConexiones().getNewConnection();
			stmt = conn.createStatement();
			String sql = "UPDATE USUARIO SET "
					+ "PASSWORD = '" + usuario.getPassword() + "', "
					+ "NOMBRE = '" + usuario.getNombre() + "', "
					+ "EDAD = " + usuario.getEdad() +", "
					+ "RECIBIR_NOTIFICACIONES = '" + usuario.getRecibirNotificacionesAsString() + "' " 
					+ " WHERE ID_USUARIO = " +usuario.getIdUsuario();
			
			return stmt.executeUpdate(sql);
			
		}
		catch(SQLException e) {
			System.err.println("Error al actualizar usuario. ");
			System.err.println("Detalle del error:  " + e.getMessage());
			e.printStackTrace();
			throw e;
		}
		finally {
			try {
				stmt.close();
			}
			catch(Exception ignore) {}
			try {
				conn.close();
			}
			catch(Exception ignore) {}
		}
	
	}
	
}














