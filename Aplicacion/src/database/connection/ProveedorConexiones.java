package database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProveedorConexiones {

	public Connection getNewConnection() throws SQLException {
		try {
			String claseDriver = "org.mariadb.jdbc.Driver";
			String urlConexion = "jdbc:mysql://localhost:3306/dam_interfaces";
			String usuario = "dam_interfaces";
			String password = "fuego";

			Class.forName(claseDriver);
			Connection conn = DriverManager.getConnection(urlConexion, usuario, password);
			return conn;
		} 
		catch (ClassNotFoundException e) {
			throw new SQLException();
		}
	}
}
