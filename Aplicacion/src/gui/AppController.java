package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import gui.view.PanelLogin;
import gui.view.PanelUsuario;
import modelo.usuarios.Usuario;
import services.usuarios.UsuariosService;
import services.usuarios.exception.AccesoDenegadoException;
import services.usuarios.exception.UsuarioNoExisteException;
import services.usuarios.exception.UsuariosServiceException;

public class AppController implements ActionListener {

	private JFrame ventana;
	private PanelLogin panelLogin;
	private PanelUsuario panelUsuario;

	public AppController() {
		ventana = new JFrame("Aplicaci�n b�sica b�sica");
		ventana.setBounds(20, 20, 400, 200);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setResizable(false);
		ventana.setLocationRelativeTo(null);

		panelLogin = new PanelLogin(this);
		panelUsuario = new PanelUsuario(this);

		ventana.setContentPane(panelLogin);
	}

	public void init() {
		ventana.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		if (comando.equals("EXIT")) {
			salir();
		} else if (comando.equals("LOGIN")) {
			login();
		} else if (comando.equals("GO_TO_LOGIN")) {
			volverToLogin();
		} else if (comando.equals("SAVE")) {
			salvarUsuario();
		}
	}

	private void salvarUsuario() {
		try {
			Usuario usuario = panelUsuario.getUsuarioActualizado();
			validarDatos(usuario);
			UsuariosService usuariosService = new UsuariosService();
			usuariosService.actualizarUsuario(usuario);
			mostrarMensaje("Usuarios", "Usuario salvado correctamente.");
			
		} catch (UsuarioNoExisteException | UsuariosServiceException e) {
			mostrarMensajeError("Error no controlado", "Se ha producido un error inesperado. Lo siento.");
		} catch (Exception e) {
			System.err.println("Error no controlado: " + e.getMessage());
			e.printStackTrace();
			mostrarMensajeError("Error no controlado", "Se ha producido un error inesperado. Lo siento.");
		}

	}

	private void validarDatos(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	private void volverToLogin() {
		ventana.setContentPane(panelLogin);
		panelLogin.resetear();
		ventana.revalidate();
		ventana.repaint();
	}

	private void login() {
		try {
			String usuario = panelLogin.getTfUsuario().getText();
			String password = new String(panelLogin.getPfPassword().getPassword());

			validarDatos(usuario, password);

			UsuariosService usuariosService = new UsuariosService();

			Long idUsuario = usuariosService.login(usuario, password);

			Usuario usuarioObj = usuariosService.consultarDatosUsuario(idUsuario);

			panelUsuario.refrescarDatosUsuario(usuarioObj);

			ventana.setContentPane(panelUsuario);
			ventana.revalidate();
		} catch (AccesoDenegadoException e) {
			mostrarMensajeError("Acceso denegado", "Usuario o contrase�a incorrecta");
		} catch (UsuarioNoExisteException | UsuariosServiceException e) {
			mostrarMensajeError("Error no controlado", "Se ha producido un error inesperado. Lo siento.");
		} catch (Exception e) {
			System.err.println("Error no controlado: " + e.getMessage());
			e.printStackTrace();
			mostrarMensajeError("Error no controlado", "Se ha producido un error inesperado. Lo siento.");
		}

	}

	private void validarDatos(String usuario, String password) {
		// TODO: validar datos obligatorios y dem�s...
	}

	private void salir() {
		System.exit(0);
	}

	private void mostrarMensaje(String titulo, String mensaje) {
		JOptionPane.showMessageDialog(ventana, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
	}

	private void mostrarMensajeError(String titulo, String mensaje) {
		JOptionPane.showMessageDialog(ventana, mensaje, titulo, JOptionPane.ERROR_MESSAGE);
	}

}
