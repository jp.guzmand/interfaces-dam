package gui.view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.AppController;
import modelo.usuarios.Usuario;


public class PanelUsuario extends JPanel {

	private static final long serialVersionUID = -5719010638948399714L;
	
	private JTextField tfUsuario;
	private JTextField tfPassword;
	private JTextField tfNombre;
	
	private Usuario usuario;
	
	
	public PanelUsuario(AppController controller) {
		JPanel formulario = new JPanel();
		add(formulario);

		GridLayout layout= new GridLayout(4, 3, 10, 10);
		formulario.setLayout(layout);
		
		JLabel lbUsuario = new JLabel("Usuario:");
		JLabel lbPassword = new JLabel("Password:");
		JLabel lbNombre = new JLabel("Nombre:");
		
		 tfUsuario = new JTextField();
		 tfPassword = new JTextField();
		 tfNombre = new JTextField();
		
		JButton btSalir = new JButton("Salir");
		JButton btGuardar = new JButton("Guardar");
		btSalir.setActionCommand("GO_TO_LOGIN");
		btGuardar.setActionCommand("SAVE");
		btSalir.addActionListener(controller);
		btGuardar.addActionListener(controller);

		formulario.add(lbUsuario);
		formulario.add(tfUsuario);
		formulario.add(lbPassword);
		formulario.add(tfPassword);
		formulario.add(lbNombre);
		formulario.add(tfNombre);
		formulario.add(btGuardar);
		formulario.add(btSalir);

	}


	public void refrescarDatosUsuario(Usuario usuarioObj) {
		usuario = usuarioObj;
		tfNombre.setText(usuarioObj.getNombre());
		tfPassword.setText(usuarioObj.getPassword());
		tfUsuario.setText(usuarioObj.getUsuario());
	}
	
	public Usuario getUsuarioActualizado() {
		usuario.setNombre(tfNombre.getText());
		usuario.setPassword(tfPassword.getText());
		usuario.setUsuario(tfUsuario.getText());
		return usuario;
	}
	

}
