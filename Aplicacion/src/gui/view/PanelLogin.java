package gui.view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import gui.AppController;

public class PanelLogin extends JPanel {

	private static final long serialVersionUID = 646041725815070707L;

	private JTextField tfUsuario;
	private JPasswordField pfPassword;

	public PanelLogin(AppController controller) {
		JPanel formulario = new JPanel();
		add(formulario);

		GridLayout layout = new GridLayout(3, 2, 10, 10);
		formulario.setLayout(layout);

		JLabel lbUsuario = new JLabel("Usuario:");
		JLabel lbPassword = new JLabel("Password:");

		tfUsuario = new JTextField();
		pfPassword = new JPasswordField();

		JButton btSalir = new JButton("Salir");
		JButton btAceptar = new JButton("Aceptar");

		btSalir.setActionCommand("EXIT");
		btAceptar.setActionCommand("LOGIN");
		btSalir.addActionListener(controller);
		btAceptar.addActionListener(controller);

		formulario.add(lbUsuario);
		formulario.add(tfUsuario);
		formulario.add(lbPassword);
		formulario.add(pfPassword);
		formulario.add(btAceptar);
		formulario.add(btSalir);
	}

	public JTextField getTfUsuario() {
		return tfUsuario;
	}

	public JPasswordField getPfPassword() {
		return pfPassword;
	}

	public void resetear() {
		tfUsuario.setText("");
		pfPassword.setText("");
	}
	
	
}
