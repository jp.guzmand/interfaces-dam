package test;

import java.sql.SQLException;

import database.usuarios.UsuariosDatabase;
import modelo.usuarios.Usuario;

public class Test {
	public static void main(String[] args) {
		try {
			UsuariosDatabase userDb = new UsuariosDatabase();
			
			Usuario usuario = userDb.consultarUsuarioPorLogin("BLAS");
	
			System.out.println(usuario);
			
			usuario.setEdad(usuario.getEdad()+1);
			
			userDb.actualizarUsuario(usuario);
			
			usuario = userDb.consultarUsuarioPorLogin("BLAS");
			
			System.out.println(usuario);
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
