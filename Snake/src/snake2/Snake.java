package snake2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Snake implements KeyListener {

	public static final String RIGHT = "RIGHT";
	public static final String LEFT = "LEFT";
	public static final String UP = "UP";
	public static final String DOWN = "DOWN";

	
	private Coordenada[] coordenadas;
	private String direccion;

	public Snake() {
		coordenadas = new Coordenada[4];
		coordenadas[0] = new Coordenada(0, 0);
		coordenadas[1] = new Coordenada(10, 0);
		coordenadas[2] = new Coordenada(20, 0);
		coordenadas[3] = new Coordenada(30, 0);
		
		direccion = RIGHT;
	}
	
	public void mover() {
		// movemos toda la serpiente hacia adelante
		for (int i = 0; i < coordenadas.length -1; i++) {
			coordenadas[i] = coordenadas[i+1];
		}
		
		int xCabeza = coordenadas[3].getX();
		int yCabeza = coordenadas[3].getY();
		
		
		Coordenada nuevaCabeza = new Coordenada(xCabeza, yCabeza);
		
		
		// ahora s�lo nos falta mover la cabeza a la direcci�n indicada
		if (direccion.equals(RIGHT) ) {
			nuevaCabeza.moverDcha();
		}
		else if (direccion.equals(LEFT)) {
			nuevaCabeza.moverIzda();
		}
		else if (direccion.equals(DOWN)) {
			nuevaCabeza.moverAbajo();
		}
		else if (direccion.equals(UP)) {
			nuevaCabeza.moverArriba();
		}
		
		coordenadas[3] = nuevaCabeza;
		
	}

	public void cambiarDireccion(String nuevaDireccion) {
		if ( (nuevaDireccion.equals(RIGHT) && direccion.equals(LEFT))
				|| (nuevaDireccion.equals(LEFT) && direccion.equals(RIGHT))
				|| (nuevaDireccion.equals(UP) && direccion.equals(DOWN))
				|| (nuevaDireccion.equals(DOWN) && direccion.equals(UP))) {
			System.out.println("Direcci�n no permitida");
			return;
		}
		this.direccion = nuevaDireccion;
	}
	
	
	
	public Coordenada[] getCoordenadas() {
		return coordenadas;
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode()==KeyEvent.VK_DOWN) {
			cambiarDireccion(Snake.DOWN);
		}
		else if (e.getKeyCode()==KeyEvent.VK_UP) {
			cambiarDireccion(Snake.UP);
		}
		else if (e.getKeyCode()==KeyEvent.VK_LEFT) {
			cambiarDireccion(Snake.LEFT);
		}
		else if (e.getKeyCode()==KeyEvent.VK_RIGHT) {
			cambiarDireccion(Snake.RIGHT);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

}
