package snake2;

import javax.swing.JFrame;

public class Lanzador {

	/*
	 * En esta versi�n se incluye una serpiente formada por 4 componentes en lugar de un s�lo punto.
	 */

	public static void main(String[] args) {
		JFrame ventana = new JFrame("Snake v2");
		ventana.setBounds(20, 20, 1000, 500);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		
		Snake snake = new Snake();
		PanelJuego panel = new PanelJuego(snake);
		ventana.setContentPane(panel);
		ventana.setVisible(true);
		ventana.addKeyListener(snake);
		
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			snake.mover();
			panel.repaint();
		}
		
		
	}

}
