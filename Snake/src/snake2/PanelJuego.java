package snake2;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class PanelJuego extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4081264039203509968L;

	private Snake snake;
	
	public PanelJuego(Snake snake) {
		this.snake = snake;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		for (int i = 0; i < snake.getCoordenadas().length; i++) {
			Coordenada coordenada = snake.getCoordenadas()[i];
			g.setColor(Color.BLUE);
			g.fillRect(coordenada.getX(), coordenada.getY(), 10, 10);
		}
		
		
	}
	

}
