package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class PanelJuego extends JPanel implements KeyListener {

	private static final String RIGHT = "RIGHT";

	private static final String LEFT = "LEFT";

	private static final String UP = "UP";

	private static final String DOWN = "DOWN";

	/**
	 * 
	 */
	private static final long serialVersionUID = 4081264039203509968L;

	private int x;
	private int y;
	private String direccion = RIGHT;
	
	public PanelJuego() {
		x = 0;
		y = 20;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		g.setColor(Color.BLUE);
		g.fillRect(x, y, 10, 10);
		
	}
	
	public void mover() {
		if (direccion.equals(RIGHT)) {
			x = x+10;
		}
		else if (direccion.equals(LEFT)) {
			x = x-10;
		}
		else if (direccion.equals(DOWN)) {
			y = y+10;
		}
		else if (direccion.equals(UP)) {
			y = y-10;
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode()==KeyEvent.VK_DOWN) {
			direccion = DOWN;
		}
		else if (e.getKeyCode()==KeyEvent.VK_UP) {
			direccion = UP;
		}
		else if (e.getKeyCode()==KeyEvent.VK_LEFT) {
			direccion = LEFT;
		}
		else if (e.getKeyCode()==KeyEvent.VK_RIGHT) {
			direccion = RIGHT;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	

}
