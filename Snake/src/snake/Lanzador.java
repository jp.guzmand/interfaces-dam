package snake;

import javax.swing.JFrame;

public class Lanzador {

	/*
	 * Versi�n b�sica de la serpiente que consiste en un �nico punto que se desplaza por el tablero
	 * seg�n las indicaciones del usuario
	 */

	public static void main(String[] args) {
		JFrame ventana = new JFrame("Snake v1");
		ventana.setBounds(20, 20, 1000, 500);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		PanelJuego panel = new PanelJuego();
		ventana.setContentPane(panel);
		ventana.setVisible(true);
		ventana.addKeyListener(panel);
		
		while (true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			panel.mover();
			panel.repaint();
		}
		
		
	}

}
