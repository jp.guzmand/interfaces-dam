package snake4;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class PanelJuego extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4081264039203509968L;

	private Snake snake;
	private Coordenada comida;
	private boolean gameOver;
	
	public PanelJuego(Snake snake) {
		this.snake = snake;
		gameOver = false;
		comida = dameNuevaComida();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		if (gameOver) {
			gameOver(g);
			return;
		}
		
		
		g.setColor(Color.RED);
		g.fillRect(comida.getX(), comida.getY(), 10, 10);
		
		for (int i = 0; i < snake.getCoordenadas().size(); i++) {
			Coordenada coordenada = snake.getCoordenadas().get(i);
			g.setColor(Color.BLUE);
			g.fillRect(coordenada.getX(), coordenada.getY(), 10, 10);
		}
		
		if (snake.getCabeza().equals(comida)) { // el m�todo equal de la clase coordenada est� sobrescrito
			comer();
		}

		// comprobamos si nos hemos salido del tablero
		if (getWidth()<snake.getCabeza().getX() || getHeight()<snake.getCabeza().getY()) {
			gameOver(g);
		}
		if (snake.getCabeza().getX() < 0 || snake.getCabeza().getY() < 0) {
			gameOver(g);
		}
		
		// comprobamos si nos hemos chocado con nosotros
		if (snake.hemosChocadoConNosotros()) {
			gameOver(g);
		}
	}
	
	private void gameOver(Graphics g) {
		gameOver = true;
		g.setColor(Color.BLACK);
		g.setFont(new Font("ARIAL", Font.BOLD, 70));
		g.drawString("GAME OVER :( ", 200, 200);
	}

	private void comer() {
		comida = dameNuevaComida();
		snake.setCrecerEnProximoMovimiento(true);
	}

	public Coordenada dameNuevaComida(){
		Random random = new Random();
		int xRandom = random.nextInt(1000/100);
		int yRandom = random.nextInt(500/100);
		Coordenada coordenada = new Coordenada(xRandom*100, yRandom*100);
		return coordenada;
	}

}
