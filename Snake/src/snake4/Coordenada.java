package snake4;

public class Coordenada {

	int x;
	int y;
	
	
	public Coordenada(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}


	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}
	
	public void moverDcha() {
		x = x+10;
	}
	public void moverIzda() {
		x = x-10;
	}
	public void moverArriba() {
		y = y-10;
	}
	public void moverAbajo() {
		y = y+10;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Coordenada) {
			Coordenada c = (Coordenada) obj;
			return x==c.getX() && y ==c.getY();
		}
		return false;
	}
	
}
