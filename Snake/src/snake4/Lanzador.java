package snake4;

import javax.swing.JFrame;

public class Lanzador {

	/*
	 * En esta versi�n se incluye control de Game Over cuando la serpiente se sale del tablero
	 * o se choca consigo misma
	 */

	public static void main(String[] args) {
		JFrame ventana = new JFrame("Snake v4");
		ventana.setBounds(20, 20, 1000, 500);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		
		Snake snake = new Snake();
		PanelJuego panel = new PanelJuego(snake);
		ventana.setContentPane(panel);
		ventana.setVisible(true);
		ventana.addKeyListener(snake);
		
		while (true) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			snake.mover();
			panel.repaint();
		}
		
		
	}

}
