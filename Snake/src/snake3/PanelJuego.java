package snake3;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class PanelJuego extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4081264039203509968L;

	private Snake snake;
	private Coordenada comida;
	
	public PanelJuego(Snake snake) {
		this.snake = snake;
		comida = dameNuevaComida();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		g.setColor(Color.RED);
		g.fillRect(comida.getX(), comida.getY(), 10, 10);
		
		for (int i = 0; i < snake.getCoordenadas().size(); i++) {
			Coordenada coordenada = snake.getCoordenadas().get(i);
			g.setColor(Color.BLUE);
			g.fillRect(coordenada.getX(), coordenada.getY(), 10, 10);
		}
		
		if (snake.getCabeza().equals(comida)) { // el m�todo equal de la clase coordenada est� sobrescrito
			comer();
		}
	
	}
	
	private void comer() {
		comida = dameNuevaComida();
		snake.setCrecerEnProximoMovimiento(true);
	}

	public Coordenada dameNuevaComida(){
		Random random = new Random();
		int xRandom = random.nextInt(1000/100);
		int yRandom = random.nextInt(500/100);
		Coordenada coordenada = new Coordenada(xRandom*100, yRandom*100);
		return coordenada;
	}

}
