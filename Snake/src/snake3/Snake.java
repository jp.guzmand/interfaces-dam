package snake3;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class Snake implements KeyListener {

	public static final String RIGHT = "RIGHT";
	public static final String LEFT = "LEFT";
	public static final String UP = "UP";
	public static final String DOWN = "DOWN";

	
	private List<Coordenada> coordenadas;
	private String direccion;
	private boolean crecerEnProximoMovimiento;

	public Snake() {
		coordenadas =  new ArrayList<>();
		coordenadas.add(new Coordenada(0, 0));
		coordenadas.add(new Coordenada(10, 0));
		coordenadas.add(new Coordenada(20, 0));
		coordenadas.add(new Coordenada(30, 0));
		
		crecerEnProximoMovimiento = false;
		
		direccion = RIGHT;
	}
	
	public void mover() {
		if (crecerEnProximoMovimiento) {
			// No movemos la serpiente, simplemente crecemos
			crecer();
			crecerEnProximoMovimiento = false;
			return;
		}
		
		int tama�o = coordenadas.size();
		
		// movemos toda la serpiente hacia adelante
		for (int i = 0; i < tama�o -1; i++) {
			coordenadas.set(i, coordenadas.get(i+1));
		}
		
		Coordenada cabeza = getCabeza();
		int xCabeza = cabeza.getX();
		int yCabeza = cabeza.getY();
		Coordenada nuevaCabeza = new Coordenada(xCabeza, yCabeza);
		
		moverCoordenada(nuevaCabeza);
		coordenadas.set(tama�o-1, nuevaCabeza);
		
	}
	public void crecer() {
		Coordenada cabeza = getCabeza();
		int xCabeza = cabeza.getX();
		int yCabeza = cabeza.getY();
		Coordenada nuevaCabeza = new Coordenada(xCabeza, yCabeza);
		
		moverCoordenada(nuevaCabeza);
		coordenadas.add(nuevaCabeza);
		
	}

	private void moverCoordenada(Coordenada nuevaCabeza) {
		if (direccion.equals(RIGHT) ) {
			nuevaCabeza.moverDcha();
		}
		else if (direccion.equals(LEFT)) {
			nuevaCabeza.moverIzda();
		}
		else if (direccion.equals(DOWN)) {
			nuevaCabeza.moverAbajo();
		}
		else if (direccion.equals(UP)) {
			nuevaCabeza.moverArriba();
		}
	}

	public void cambiarDireccion(String nuevaDireccion) {
		if ( (nuevaDireccion.equals(RIGHT) && direccion.equals(LEFT))
				|| (nuevaDireccion.equals(LEFT) && direccion.equals(RIGHT))
				|| (nuevaDireccion.equals(UP) && direccion.equals(DOWN))
				|| (nuevaDireccion.equals(DOWN) && direccion.equals(UP))) {
			System.out.println("Direcci�n no permitida");
			return;
		}
		this.direccion = nuevaDireccion;
	}
	
	
	
	public List<Coordenada> getCoordenadas() {
		return coordenadas;
	}

	public Coordenada getCabeza() {
		return coordenadas.get(coordenadas.size()-1);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode()==KeyEvent.VK_DOWN) {
			cambiarDireccion(Snake.DOWN);
		}
		else if (e.getKeyCode()==KeyEvent.VK_UP) {
			cambiarDireccion(Snake.UP);
		}
		else if (e.getKeyCode()==KeyEvent.VK_LEFT) {
			cambiarDireccion(Snake.LEFT);
		}
		else if (e.getKeyCode()==KeyEvent.VK_RIGHT) {
			cambiarDireccion(Snake.RIGHT);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	public void setCrecerEnProximoMovimiento(boolean crecer) {
		crecerEnProximoMovimiento = crecer;
	}

}
