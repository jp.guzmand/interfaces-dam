package jtable.modelo;

public class Libro {

	private String titulo;
	private String autor;
	private Integer a�o;
	
	public Libro() {
	}

	public Libro(String titulo, String autor, Integer a�o) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.a�o = a�o;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Integer getA�o() {
		return a�o;
	}

	public void setA�o(Integer a�o) {
		this.a�o = a�o;
	}

	
}
