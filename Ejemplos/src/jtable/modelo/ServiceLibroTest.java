package jtable.modelo;

import java.util.ArrayList;
import java.util.List;

public class ServiceLibroTest {

	public ServiceLibroTest() {
	}

	
	public List<Libro> getListaLibros(){
		List<Libro> libros = new ArrayList<Libro>();
		libros.add(new Libro("El discurso del m�todo", "Descartes", 1500));
		libros.add(new Libro("As� habl� Zaratustra", "Nietzsche", 1850));
		libros.add(new Libro("El ser y la nada", "Sartre", 1950));
		libros.add(new Libro("Cr�tica a la raz�n pura", "Kant", 1700));
		libros.add(new Libro("El capital", "Marx", 1850));
		libros.add(new Libro("�tica a Nic�maco", "Arist�teles", 0));
		libros.add(new Libro("M�s all� del bien y del mal", "Nietzsche", 1880));
		return libros;
	}
	
}
