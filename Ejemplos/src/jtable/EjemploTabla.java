package jtable;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import jtable.modelo.Libro;
import jtable.modelo.ServiceLibroTest;

public class EjemploTabla extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8906999030259440166L;
	
	private JPanel contentPane;
	private JTable table;
	private JButton btnConsultar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EjemploTabla frame = new EjemploTabla();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EjemploTabla() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		

		
		EjemploTableModel model = new EjemploTableModel();
		table = new JTable(model);
		JScrollPane scrollPane = new JScrollPane(table); 
		table.setFillsViewportHeight(true); // Rellena la tabla en todo el panel
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				ServiceLibroTest serviceLibros = new ServiceLibroTest();
				List<Libro> listaLibros = serviceLibros.getListaLibros(); // Nos devuelve una lista de libros de ejemplo para probar
				model.setDatos(listaLibros);
				model.fireTableDataChanged(); // Notifica que han cambiado los datos, para que la tabla se refresque
			}
		});
		contentPane.add(btnConsultar, BorderLayout.NORTH);

		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				Integer rowClicked = table.rowAtPoint(e.getPoint()); // Nos devuelve la fila donde se ha hecho click
				String tituloClicked = (String) table.getModel().getValueAt(rowClicked, 0); // Nos devuelve el valor de la celda indicada
				System.out.println(tituloClicked);
			}
		});
		
		
	}

}
