package jtable;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import jtable.modelo.Libro;

public class EjemploTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1124820336342698518L;

	private List<Libro> datos;

	public EjemploTableModel() {
		datos = new ArrayList<Libro>();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		return datos.size();
	}

	public String getColumnName(int col) {
        switch (col) {
		case 0:
			return "T�tulo";
		case 1:
			return "Autor";
		case 2:
			return "A�o";
		}
    	return "";
    }

	@Override
	public Object getValueAt(int row, int column) {
		Libro libro = datos.get(row);
        switch (column) {
		case 0:
			return libro.getTitulo();
		case 1:
			return libro.getAutor();
		case 2:
			return libro.getA�o();
		}
    	return "";
	}
	
	public void setDatos(List<Libro> datos) {
		this.datos = datos;
	}
	
}
