package navegacion3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import navegacion3.pantallas.PanelEstasDentro;
import navegacion3.pantallas.PanelEstasFuera;
import navegacion3.pantallas.PanelMenu;


public class AppController implements ActionListener {

	private JFrame ventanaMenu;
	private JFrame ventanaGeneral;
	private PanelMenu panelMenu;
	private PanelEstasDentro panelDentro;
	private PanelEstasFuera panelFuera;
	
	public AppController() {
		ventanaMenu = new JFrame("Ejemplo navegaci�n men�");
		ventanaMenu.setBounds(20, 20, 400, 400);
		ventanaMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ventanaGeneral = new JFrame("Ejemplo navegaci�n general");
		ventanaGeneral.setBounds(100, 100, 600, 600);
		ventanaGeneral.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		
		panelMenu = new PanelMenu(this);
		panelDentro = new PanelEstasDentro(this);
		panelFuera = new PanelEstasFuera(this);
		
		ventanaMenu.setContentPane(panelMenu);
	}

	public void init() {
		ventanaMenu.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		if (comando.equals("GO_VIEW_DENTRO")) {
			ventanaGeneral.setVisible(true);
			ventanaMenu.setVisible(false);
			ventanaGeneral.setContentPane(panelDentro);
			ventanaGeneral.revalidate();
		}
		if (comando.equals("GO_VIEW_FUERA")) {
			ventanaGeneral.setVisible(true);
			ventanaMenu.setVisible(false);
			ventanaGeneral.setContentPane(panelFuera);
			ventanaGeneral.revalidate();
		}
		if (comando.equals("GO_VIEW_MENU")) {
			ventanaMenu.setVisible(true);
			ventanaGeneral.setVisible(false);
			ventanaMenu.setContentPane(panelMenu);
			ventanaMenu.revalidate();
		}
	}
	
	

}
