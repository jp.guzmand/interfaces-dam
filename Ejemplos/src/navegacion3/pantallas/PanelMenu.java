package navegacion3.pantallas;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import navegacion3.controlador.AppController;

public class PanelMenu extends JPanel {

	private static final long serialVersionUID = 646041725815070707L;

	public PanelMenu(AppController controller) {
		GridLayout layout= new GridLayout(2, 1);
		setLayout(layout);

		JButton bt1 = new JButton();
		bt1.setText("Entrar");
		bt1.setActionCommand("GO_VIEW_DENTRO");
		add(bt1);

		JButton bt2 = new JButton();
		bt2.setText("Salir");
		bt2.setActionCommand("GO_VIEW_FUERA");
		add(bt2);

		bt1.addActionListener(controller);
		bt2.addActionListener(controller);
	}



}
