package navegacion3.pantallas;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import navegacion3.controlador.AppController;


public class PanelEstasDentro extends JPanel {

	private static final long serialVersionUID = 646041725815070707L;
	
	public PanelEstasDentro(AppController controller) {
		GridLayout layout= new GridLayout(2, 1);
		setLayout(layout);

		JLabel lbMsg = new JLabel();
		lbMsg.setText("<html><div style='color:green;'>Est�s dentro</div></html>");
		lbMsg.setHorizontalAlignment(JLabel.CENTER);
		add(lbMsg);

		JButton btVolver = new JButton();
		btVolver.setActionCommand("GO_VIEW_MENU");
		btVolver.setText("Volver");
		add(btVolver);

		btVolver.addActionListener(controller);
	}


}
