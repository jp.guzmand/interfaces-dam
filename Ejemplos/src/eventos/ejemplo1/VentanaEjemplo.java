package eventos.ejemplo1;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VentanaEjemplo extends JFrame implements ActionListener{

	private static final long serialVersionUID = 2713328608861704121L;
	private JLabel lbMensaje;

	public VentanaEjemplo(String titulo) {
		super(titulo);
		setBounds(20, 20, 400, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		GridLayout layout= new GridLayout(4, 1);
		panel.setLayout(layout);
		add(panel);

		JButton bt1 = new JButton();
		bt1.setText("1");
		panel.add(bt1);

		JButton bt2 = new JButton();
		bt2.setText("2");
		panel.add(bt2);

		JButton bt3 = new JButton();
		bt3.setText("3");
		panel.add(bt3);

		lbMensaje  = new JLabel();
		lbMensaje.setText("");
		panel.add(lbMensaje);

		// A�adimos un listener que hemos implementado en otra clase
		ActionListener botonListener = new BotonActionListener(lbMensaje);
		bt1.addActionListener(botonListener);
		
		// A�adimos un listener que hemos implementado en esta misma clase
		bt2.addActionListener(this);
		
		// A�adimos un listener que definimos al instanciarlo
		bt3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lbMensaje.setText("Has hecho click en el bot�n 3");
			}
		});
		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		lbMensaje.setText("Has hecho click en el bot�n 2");
	}

}
