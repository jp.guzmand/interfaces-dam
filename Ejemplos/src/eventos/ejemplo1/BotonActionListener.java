package eventos.ejemplo1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class BotonActionListener implements  ActionListener{

	private JLabel lbMensaje;
	
	// El constructor del Listener recibe el label donde mostraremos un mensaje
	public BotonActionListener(JLabel lbMensaje) {
		this.lbMensaje = lbMensaje;
	}

	@Override // Este es el m�todo que se dispara cuando se genera el evento
	public void actionPerformed(ActionEvent e) {
		lbMensaje.setText("Has hecho click en el bot�n 1");
	}
	
}
