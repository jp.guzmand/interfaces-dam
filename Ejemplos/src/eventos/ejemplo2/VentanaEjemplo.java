package eventos.ejemplo2;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VentanaEjemplo extends JFrame {

	private static final long serialVersionUID = 2713328608861704121L;
	private JLabel lbMensaje;

	public VentanaEjemplo(String titulo) {
		super(titulo);
		setBounds(20, 20, 400, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		GridLayout layout= new GridLayout(4, 1);
		panel.setLayout(layout);
		add(panel);

		JButton bt1 = new JButton();
		bt1.setText("1");
		bt1.setActionCommand("boton1");
		panel.add(bt1);

		JButton bt2 = new JButton();
		bt2.setText("2");
		bt2.setActionCommand("boton2");
		panel.add(bt2);

		JButton bt3 = new JButton();
		bt3.setText("3");
		bt3.setActionCommand("boton3");
		panel.add(bt3);

		lbMensaje  = new JLabel();
		lbMensaje.setText("");
		panel.add(lbMensaje);

		// A�adimos un listener que hemos implementado en otra clase
		BotonActionListener botonListener = new BotonActionListener(lbMensaje);
		bt1.addActionListener(botonListener);
		bt2.addActionListener(botonListener);
		bt3.addActionListener(botonListener);
		
		bt1.addMouseListener(botonListener);
		bt2.addMouseListener(botonListener);
		bt3.addMouseListener(botonListener);
		

	}
}
