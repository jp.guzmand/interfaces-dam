package eventos.ejemplo2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class BotonActionListener implements  ActionListener, MouseListener{

	private JLabel lbMensaje;
	
	// El constructor del Listener recibe el label donde mostraremos un mensaje
	public BotonActionListener(JLabel lbMensaje) {
		this.lbMensaje = lbMensaje;
	}

	@Override // Este es el m�todo que se dispara cuando se genera el evento
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.contentEquals("boton1")) {
			lbMensaje.setText("Has hecho click en el bot�n 1");
		}
		else if (command.contentEquals("boton2")) {
			lbMensaje.setText("Has hecho click en el bot�n 2");
		}
		else if (command.contentEquals("boton3")) {
			lbMensaje.setText("Has hecho click en el bot�n 3");
		}
		
		JButton boton = (JButton) e.getSource();
		boton.setText(boton.getText() + "!");
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		JButton boton = (JButton) e.getSource();
		boton.setBackground(Color.GREEN);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		JButton boton = (JButton) e.getSource();
		boton.setBackground(null);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}
	
}
