package focuslistener;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class MiVentana extends JFrame implements FocusListener, ActionListener{

	private static final String VERDE = "Verde";
	private static final String AZUL = "Azul";
	private static final String ROJO = "Rojo";

	/**
	 * 
	 */
	private static final long serialVersionUID = -4527052581265583002L;
	
	private String colorSeleccionado = ROJO;

	public MiVentana() {
		setBounds(20, 20, 800, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		JPanel formulario = new JPanel();
		add(formulario);
		
		JTextField t1 = new JTextField(20);
		JTextField t2 = new JTextField(20);
		JTextField t3 = new JTextField(20);

		t1.addFocusListener(this);
		t2.addFocusListener(this);
		t3.addFocusListener(this);
		
		JComboBox<String> combo = new JComboBox<String>();
		combo.addItem(ROJO);
		combo.addItem(AZUL);
		combo.addItem(VERDE);

		JRadioButton rbRojo = new JRadioButton(ROJO);
		rbRojo.addActionListener(this);
	    JRadioButton rbAzul = new JRadioButton(AZUL);
	    rbAzul.addActionListener(this);
	    JRadioButton rbVerde = new JRadioButton(VERDE);
	    rbVerde.addActionListener(this);

	    //Group the radio buttons.
	    ButtonGroup group = new ButtonGroup();
	    group.add(rbRojo);
	    group.add(rbAzul);
	    group.add(rbVerde);

		
		formulario.add(combo);
		formulario.add(t1);
		formulario.add(t2);
		formulario.add(t3);
		formulario.add(rbRojo);
		formulario.add(rbAzul);
		formulario.add(rbVerde);
		
		combo.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton) {
			JRadioButton combo = (JRadioButton) e.getSource();
			colorSeleccionado = (String) combo.getText();
		}
		else if (e.getSource() instanceof JComboBox) {
			JComboBox<String> combo = (JComboBox<String>) e.getSource();
			colorSeleccionado = (String) combo.getSelectedItem(); 
		}
	}

	@Override
	public void focusGained(FocusEvent e) {
		JTextField tf = (JTextField) e.getSource();
		tf.setBackground(getColorSeleccionado());
	}


	@Override
	public void focusLost(FocusEvent e) {
		JTextField tf = (JTextField) e.getSource();
		tf.setBackground(Color.WHITE);
	}

	
	private Color getColorSeleccionado() {
		if (colorSeleccionado.equals(AZUL)) {
			return Color.BLUE;
		}
		if (colorSeleccionado.equals(ROJO)) {
			return Color.RED;
		}
		if (colorSeleccionado.equals(VERDE)) {
			return Color.GREEN;
		}
		return null;
	}


}
