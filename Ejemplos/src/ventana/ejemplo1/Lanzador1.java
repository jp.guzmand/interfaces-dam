package ventana.ejemplo1;

import javax.swing.JFrame;

public class Lanzador1 {

	public static void main(String[] args) {

		// Instancio la clase
		JFrame miPrimeraVentana = new JFrame();
		
		// Le pongo un t�tulo a la ventana
		miPrimeraVentana.setTitle("T�tulo");
		
		// Cambio la posici�n donde se mostrar� (los dos primeros par�metros)
		// Cambio el tama�o (los dos �ltimos par�metros)
		miPrimeraVentana.setBounds(100, 100, 800, 600); 
		
		// Indico que quiero que haga la ventana cuando se cierre
		// EXIT_ON_CLOSE = Terminar el programa
		miPrimeraVentana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Muestro la ventana
		miPrimeraVentana.setVisible(true);
		
	}
}
