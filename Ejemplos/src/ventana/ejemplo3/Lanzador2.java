package ventana.ejemplo3;

public class Lanzador2 {

	public static void main(String[] args) {

		// Instancio la clase
		MiVentana miPrimeraVentana = new MiVentana();
		
		// A�adimos componentes
		miPrimeraVentana.inicializaComponentes();
		
		// Muestro la ventana
		miPrimeraVentana.setVisible(true);
		
	}
}
