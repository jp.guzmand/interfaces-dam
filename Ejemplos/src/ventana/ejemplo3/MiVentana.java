package ventana.ejemplo3;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MiVentana extends JFrame {

	private static final long serialVersionUID = 4752257795598990317L;

	public MiVentana() {
		// Le pongo un t�tulo a la ventana
		setTitle("T�tulo");
		
		// Cambio la posici�n donde se mostrar� (los dos primeros par�metros)
		// Cambio el tama�o (los dos �ltimos par�metros)
		setBounds(100, 100, 400, 300); 
		
		// Indico que quiero que haga la ventana cuando se cierre
		// EXIT_ON_CLOSE = Terminar el programa
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void inicializaComponentes() {

		Container contenedor = getContentPane();

		// Layout del contenedor principal
		GridLayout layout = new GridLayout(3, 2); 
		contenedor.setLayout(layout);
		
		// Componentes de las 5 primeras celdas
		JLabel texto1 = new JLabel("Texto1");
		contenedor.add(texto1);
		JLabel texto2 = new JLabel("Texto2");
		contenedor.add(texto2);
		JLabel texto3 = new JLabel("Texto3");
		contenedor.add(texto3);
		JLabel texto4 = new JLabel("Texto4");
		contenedor.add(texto4);
		JLabel texto5 = new JLabel("Texto5");
		contenedor.add(texto5);

		// Panel de la �ltima celda
		JPanel panel = new JPanel();
		GridLayout layoutPanel = new GridLayout(2, 2); 
		panel.setLayout(layoutPanel);
		contenedor.add(panel);
		
		// Componentes para el panel de la �ltima celda
		JLabel texto61 = new JLabel("Texto61");
		panel.add(texto61);
		JLabel texto62 = new JLabel("Texto62");
		panel.add(texto62);
		JLabel texto63 = new JLabel("Texto63");
		panel.add(texto63);
		JLabel texto64 = new JLabel("Texto64");
		panel.add(texto64);
		
	}
}
