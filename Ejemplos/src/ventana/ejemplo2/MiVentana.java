package ventana.ejemplo2;

import javax.swing.JFrame;

public class MiVentana extends JFrame {

	private static final long serialVersionUID = 4752257795598990317L;

	public MiVentana() {
		// Le pongo un t�tulo a la ventana
		setTitle("T�tulo");
		
		// Cambio la posici�n donde se mostrar� (los dos primeros par�metros)
		// Cambio el tama�o (los dos �ltimos par�metros)
		setBounds(100, 100, 800, 600); 
		
		// Indico que quiero que haga la ventana cuando se cierre
		// EXIT_ON_CLOSE = Terminar el programa
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
