package navegacion;

import javax.swing.JFrame;

import navegacion.pantallas.PanelEstasDentro;
import navegacion.pantallas.PanelEstasFuera;
import navegacion.pantallas.PanelMenu;

public class VentanaPrincipal extends JFrame {

	private static final long serialVersionUID = 2713328608861704121L;

	private PanelMenu panelMenu;
	private PanelEstasDentro panelDentro;
	private PanelEstasFuera panelFuera;
	
	public VentanaPrincipal(String titulo) {
		super(titulo);
		setBounds(20, 20, 400, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		panelMenu = new PanelMenu(this);
		panelDentro = new PanelEstasDentro(this);
		panelFuera = new PanelEstasFuera(this);
		
		setContentPane(panelMenu);
	}


	public void entrar() {
		setContentPane(panelDentro);
		revalidate(); // Con este m�todo forzamos que se renderice todo de nuevo
	}

	public void salir() {
		setContentPane(panelFuera);
		revalidate();
	}

	public void mostrarMenu() {
		setContentPane(panelMenu);
		revalidate();
	}
	

}
