package navegacion.pantallas;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import navegacion.VentanaPrincipal;

public class PanelMenu extends JPanel implements ActionListener{

	private static final long serialVersionUID = 646041725815070707L;

	private VentanaPrincipal ventana;
	
	public PanelMenu(VentanaPrincipal ventana) {
		this.ventana = ventana;
		GridLayout layout= new GridLayout(2, 1);
		setLayout(layout);

		JButton bt1 = new JButton();
		bt1.setText("Entrar");
		bt1.setActionCommand("ENTRAR");
		add(bt1);

		JButton bt2 = new JButton();
		bt2.setText("Salir");
		bt2.setActionCommand("SALIR");
		add(bt2);

		bt1.addActionListener(this);
		bt2.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		if (comando.equals("ENTRAR")) {
			ventana.entrar();
		}
		else if (comando.equals("SALIR")) {
			ventana.salir();
		}
	}

}
