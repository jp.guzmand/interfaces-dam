package navegacion.pantallas;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import navegacion.VentanaPrincipal;

public class PanelEstasFuera extends JPanel implements ActionListener{

	private static final long serialVersionUID = 646041725815070707L;

	private VentanaPrincipal ventana;
	
	public PanelEstasFuera(VentanaPrincipal ventana) {
		this.ventana = ventana;
		GridLayout layout= new GridLayout(2, 1);
		setLayout(layout);

		JLabel lbMsg = new JLabel();
		lbMsg.setText("<html><div style='color:red;'>Est�s fuera</div></html>");
		lbMsg.setHorizontalAlignment(JLabel.CENTER);
		add(lbMsg);

		JButton btVolver = new JButton();
		btVolver.setText("Volver");
		add(btVolver);

		btVolver.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ventana.mostrarMenu();
	}

}
