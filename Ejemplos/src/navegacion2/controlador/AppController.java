package navegacion2.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import navegacion2.pantallas.PanelEstasDentro;
import navegacion2.pantallas.PanelEstasFuera;
import navegacion2.pantallas.PanelMenu;


public class AppController implements ActionListener {

	private JFrame ventana;
	private PanelMenu panelMenu;
	private PanelEstasDentro panelDentro;
	private PanelEstasFuera panelFuera;
	
	public AppController() {
		ventana = new JFrame("Ejemplo navegación");
		ventana.setBounds(20, 20, 400, 400);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panelMenu = new PanelMenu(this);
		panelDentro = new PanelEstasDentro(this);
		panelFuera = new PanelEstasFuera(this);
		
		ventana.setContentPane(panelMenu);
	}

	public void init() {
		ventana.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		if (comando.equals("GO_VIEW_DENTRO")) {
			ventana.setContentPane(panelDentro);
		}
		if (comando.equals("GO_VIEW_FUERA")) {
			ventana.setContentPane(panelFuera);
		}
		if (comando.equals("GO_VIEW_MENU")) {
			ventana.setContentPane(panelMenu);
		}
		ventana.revalidate();
	}
	
	

}
