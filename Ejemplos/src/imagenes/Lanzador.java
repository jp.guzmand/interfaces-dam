package imagenes;

import javax.swing.JFrame;

public class Lanzador {


	public static void main(String[] args) {
		JFrame ventana = new JFrame("Ejemplo dibujo de formas");
		ventana.setBounds(20, 20, 800, 800);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		PanelImagenes panel = new PanelImagenes();
		ventana.setContentPane(panel);
		ventana.setVisible(true);
		
		
	}

}
