package animacion;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;

public class PanelImagenes extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4081264039203509968L;

	private int x;
	private int y;
	
	public PanelImagenes() {
		x = -200;
		y = 20;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Toolkit t = Toolkit.getDefaultToolkit();
		Image imagen = t.getImage("src/imagenes/resources/blas.png");
		g.drawImage(imagen, x, y, this);
	}
	
	public void mover() {
		x = x+10;
	}

}
