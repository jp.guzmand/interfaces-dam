package animacion;

import javax.swing.JFrame;

public class Lanzador {


	public static void main(String[] args) {
		JFrame ventana = new JFrame("Ejemplo dibujo de formas");
		ventana.setBounds(20, 20, 1000, 500);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		PanelImagenes panel = new PanelImagenes();
		ventana.setContentPane(panel);
		ventana.setVisible(true);
		
		while (true) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			panel.mover();
			panel.repaint();
		}
		
	}

}
