package test;

import javax.swing.JFrame;
import javax.swing.JPanel;

import es.ceu.andalucia.dam.interfaces.numeros.JNumberField;

public class Test {


	public static void main(String[] args) {
		JFrame ventana = new JFrame("");
		ventana.setBounds(20, 20, 300, 300);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setLocationRelativeTo(null); // Nos permite abrir la ventana centrada
		JPanel panel = new JPanel();
		ventana.add(panel);
		
		JNumberField nfNumero = new JNumberField(10);
		panel.add(nfNumero);
		
		ventana.setVisible(true);
	}

}
