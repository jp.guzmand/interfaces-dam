package es.ceu.andalucia.dam.interfaces.numeros;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class JNumberField extends JTextField implements KeyListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2462529892359329856L;

	public JNumberField() {
		super();
		addKeyListener(this);
	}

	public JNumberField(Document doc, Integer number, int columns) {
		super(doc, number.toString(), columns);
		addKeyListener(this);
	}
	
	public JNumberField(int columns) {
		super(columns);
		addKeyListener(this);
	}

	public JNumberField(Integer number, int columns) {
		super(number.toString(), columns);
		addKeyListener(this);
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			setNumber(getNumber() + 1);
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			setNumber(getNumber() - 1);
		}
	}


	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
		char caracter = e.getKeyChar();
		if (caracter < '0' || caracter > '9') {
			e.consume();
		}
	}
	
	public Integer getNumber() {
		try {
			return Integer.parseInt(getText());
		}
		catch(Exception e) {
			return 0;
		}
	}

	public void setNumber(Integer i) {
		super.setText(i.toString());
	}

	

}
